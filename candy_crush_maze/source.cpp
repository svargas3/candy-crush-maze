#include "library.h"

int rowA, colA, rowB, colB, prev_rowR, prev_colR, rowR, colR, rowM, colM;
int scale = 30;
const int rowLength = 11;
const int colLength = 24; 
string maze[rowLength];
char prev_c,ch, current_dir = 'W';
char dirs[rowLength][colLength];
image * tiffi = image_from_file("tiffi.gif");
image * treasure = image_from_file("candy.gif");
image * chocolate = image_from_file("chocolate.bmp");
image * troll = image_from_file("troll.bmp");

void read_file()
{
  ifstream file("map.txt");
  if(file.fail()) 
  {
    cerr << "could not open file\n";
  }
  int  num = 0;
  while(true)
  {
    file >> maze[num];
	num++;
    if(file.fail())
     break;
  }
  file.close();
}

//Finds start and end points
void find()
{
	for(int r = 0; r < rowLength; r++)
	{
		for(int c = 0; c < colLength; c++)
		{
			if(maze[r][c]=='A')
			{
				rowA = r;
				colA = c;
			}
			if(maze[r][c]=='B')
			{
				rowB = r;
				colB = c;
			}
		}
	}
}

void square(double x, double y, double size)
{ 
	set_pen_color(color::black);
	move_to(x,y);
	draw_to(x+size,y);
	draw_to(x+size,y+size);
	draw_to(x,y+size);
	draw_to(x,y);
}

void draw_grid()
{
	window * main_window = make_window(720,380);
	main_window->set_caption("Candy Crush Maze");
	fill_rectangle(0.0, 330.0, 720.0, 430.0, color::light_grey);
	move_to(175, 360);
	set_font_size(30);
	set_pen_color(color::brown);
	write_string("Use arrow keys or WASD to move");
	for(int r = 0; r < rowLength; r++)
	{
		for(int c = 0; c < colLength; c++)
		{
			if(maze[r][c]=='A')
			{
				draw_image(tiffi, c*scale, r*scale);
			}
			if(maze[r][c]=='B')
			{
				draw_image(treasure, c*scale, r*scale);
			}
			if(maze[r][c]=='@')
			{
				draw_image(chocolate, c*scale , r*scale); //wall made of chocolate
			}
			square(c*scale,r*scale,scale);
		}
	}
}

bool isWall(int row, int col)
{
	if(maze[row][col] == '@')
		return true;
	else
		return false;
}

bool didWin()
{
	if(rowR == rowB && colR == colB)
	{
		move_to(200,360);
		fill_rectangle(0.0, 330.0, 720.0, 430.0, color::light_grey);
		set_font_size(30);
		set_pen_color(color::brown);
		write_string("Congratulations! You Win!");
		return true;
	}
	return false;
}
void move()
{
	fill_rectangle(prev_colR*scale,prev_rowR*scale,scale,scale,color::white); 
	square(prev_colR*scale,prev_rowR*scale,scale);
	draw_image(tiffi, colR*scale, rowR*scale);  
	square(colR*scale,rowR*scale,scale);
	prev_rowR = rowR;
	prev_colR = colR;
}


void isValid()
{
	bool wall = isWall(rowR, colR);
	if(wall == true)
	{
		rowR = prev_rowR;
		colR = prev_colR;
	}
	else
		move();
}

bool teleport()
{
	rowR = random_in_range(0, 10);
    colR = random_in_range(0,23);
	bool wall = isWall(rowR, colR);
	if(wall == true)
	{
		tiffi = image_from_file("tiffi_lose.gif"); 
		move();
		return false;
	}
	else
	{
		move();
		return true;
	}
}
void print()
{
	for(int r = 0; r < rowLength; r++)
	{
		for(int c = 0; c < colLength;c++)
		{
			cout << dirs[r][c] << " ";
		}
		new_line();
	}
}
bool hasBeen(char dir)
{
	if(dir == 'N' || dir == 'E' || dir == 'S' || dir == 'W')
		return true;
	else
		return false;
}
int spaces[4];
void check_around()
{
	if(isWall(rowR-1,colR) || hasBeen(dirs[rowR-1][colR]))
		spaces[0] = 0;
	else
		spaces[0] = 1;
	if(isWall(rowR,colR-1) || hasBeen(dirs[rowR][colR-1]))
		spaces[1] = 0;
	else
		spaces[1] = 1;
	if(isWall(rowR+1,colR) || hasBeen(dirs[rowR+1][colR]))
		spaces[2] = 0;
	else
		spaces[2] = 1;
	if(isWall(rowR,colR+1) || hasBeen(dirs[rowR][colR+1]))
		spaces[3] = 0;
	else
		spaces[3] = 1;
}

void back()
{
	current_dir = dirs[rowR][colR];
	if (current_dir == 'N')
		rowR += 1;
	else if (current_dir == 'W')
		colR += 1;
	else if (current_dir == 'S')
		rowR -= 1;
	else if (current_dir == 'E')
		colR -= 1;

	current_dir = dirs[rowR][colR];

	if(isWall(rowR,colR) == false)
		move();
	else
	{
		rowR = prev_rowR;
		colR = prev_colR;
	}
}

void auto_mode()
{
	char c = 'x';
	while(c != 'M')
	{
		c = wait_for_key_typed(0.15);
		int side = 0, rand, num_valid = 0, tempR, tempC;
		char temp_dir = current_dir, test;
		dirs[rowA][colA] = current_dir;
		check_around();
		for(int i = 0; i < 4; i++)
			num_valid += spaces[i];
		if(num_valid == 0)
		{
			print();
			back();
		}
		else
		{
			tempR = rowR, tempC = colR;
			while(side == 0)
			{
				rand = random_in_range(1,4);

				if(rand == 1)
				{
					if(spaces[0] == 1)
					{
						temp_dir = 'N';
						tempR -= 1;
					side = 1;
					}
				}
				else if(rand == 2)
				{
					if(spaces[1] == 1)
					{
						temp_dir = 'W';
						tempC -= 1;
						side = 1;
					}
				}
				else if(rand == 3)
				{
					if(spaces[2] == 1)
				{
					temp_dir = 'S';
					tempR += 1;
					side = 1;
					}
				}
				else if(rand == 4)
				{
					if(spaces[3] == 1)
					{
						temp_dir = 'E';
						tempC += 1;
						side = 1;}
					}
				}
		}
		
		if(num_valid != 0)
		{
			rowR = tempR;
			colR = tempC;
	
			current_dir = temp_dir;
			dirs[rowR][colR] = current_dir;
			move();
		}
		
		if(didWin())
			return;
		for(int i = 0; i < 4; i++)
			spaces[i] = ' ';
	}
}



void yellow_brick_road()
{
	char cur_dir = dirs[rowB][colB];
	
	int curR=rowB, curC=colB;
	bool start = false;
	while(!start)
	{
		cout << cur_dir << endl;
		if(cur_dir == 'N')
			curR += 1;
		else if (cur_dir == 'W')
			curC += 1;
		else if (cur_dir == 'S')
			curR -= 1;
		else if (cur_dir == 'E')
			curC -= 1;
		if(curR == rowA && curC == colA)
			start= true;
		fill_rectangle(curC*scale,curR*scale,scale,scale,color::yellow); 
		square(curC*scale,curR*scale,scale);
		cur_dir = dirs[curR][curC];
	}
	fill_rectangle(curC*scale,curR*scale,scale,scale,color::yellow); 
	square(curC*scale,curR*scale,scale);

}

void main()
{
	read_file();	
	find();
	draw_grid();
	rowR = rowA, prev_rowR = rowA;
	colR = colA, prev_colR = colA;
	
	while (true)
	{	
		ch = wait_for_key_typed();
		if (ch == 'w' || ch == -90)
		{ 
			rowR--;
			current_dir = 'N';
		}
		else if (ch == 'a' || ch == -91)
		{
			colR--;
			current_dir = 'W';
		}
		else if (ch == 's' || ch == -88)
		{
			rowR++;
			current_dir = 'S';
		}
		else if (ch == 'd' || ch == -89)
		{
			colR++;
			current_dir = 'E';
		} 
		else if(ch == 't')
		{
			bool success = teleport();
			if(success == true)
			{
				move();
			}
			else
			{
				move_to(250,360);
				fill_rectangle(0.0, 330.0, 720.0, 430.0, color::light_grey);
				set_font_size(30);
				set_pen_color(color::dark_red);
				write_string("Game Over. You lose.");
				break;
			}
		}
		else if(ch == 'A'||ch == 'a')
		{
			check_around();
			auto_mode();
		}
		else if(ch == 'B'||ch=='b')
			back();
		else if (ch == 'q' || ch == 'x')
			break;
		if(ch != 'B')
			prev_c = ch;
		
			dirs[rowR][colR] = current_dir;
		isValid();
		if(didWin())
		{
			yellow_brick_road();
			break;
		}
	}
}